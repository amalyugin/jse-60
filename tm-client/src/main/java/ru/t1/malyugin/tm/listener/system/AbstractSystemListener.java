package ru.t1.malyugin.tm.listener.system;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.listener.AbstractListener;
import ru.t1.malyugin.tm.service.PropertyService;

public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected PropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @Override
    public @NotNull Role[] getRoles() {
        return ArrayUtils.toArray();
    }

}