package ru.t1.malyugin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "exit";

    @NotNull
    private static final String DESCRIPTION = "Close Application";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}