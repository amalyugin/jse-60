package ru.t1.malyugin.tm.dto.response.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.response.AbstractResultResponse;

public class ServerErrorResponse extends AbstractResultResponse {

    public ServerErrorResponse() {
        setSuccess(false);
    }

    public ServerErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}