package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.ILoggerService;
import ru.t1.malyugin.tm.api.IPropertyService;

import java.util.LinkedHashMap;
import java.util.Map;

@Primary
@Component("loggerToMongoService")
public final class LoggerToMongoService implements ILoggerService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient;

    @Autowired
    public LoggerToMongoService(
            @NotNull final MongoClient mongoClient,
            @NotNull final IPropertyService propertyService
    ) {
        this.mongoClient = mongoClient;
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final String mongoDBName = propertyService.getMongoDBName();
        @NotNull final MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoDBName);
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}