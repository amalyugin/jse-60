package ru.t1.malyugin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.malyugin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.dto.request.system.HostNameRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerInfoRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.malyugin.tm.dto.response.system.HostNameResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerAboutResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerInfoResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.InetAddress;

import static ru.t1.malyugin.tm.util.FormatUtil.formatBytes;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();

        response.setServerName(propertyService.getApplicationName());
        response.setAuthorEmail(propertyService.getAuthorEmail());
        response.setAuthorName(propertyService.getAuthorName());
        response.setGitBranch(propertyService.getGitBranch());
        response.setGitCommitId(propertyService.getGitCommitId());
        response.setGitCommitterEmail(propertyService.getGitCommitterEmail());
        response.setGitCommitterName(propertyService.getGitCommitterName());
        response.setGitMessage(propertyService.getGitCommitMessage());
        response.setGitTime(propertyService.getGitCommitTime());

        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();

        response.setVersion(propertyService.getApplicationVersion());

        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerInfoResponse getInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerInfoRequest request
    ) {
        @NotNull final ServerInfoResponse response = new ServerInfoResponse();

        final int processorsCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        response.setProcessorsCount(processorsCount);
        response.setMaxMemory(formatBytes(maxMemory));
        response.setTotalMemory(formatBytes(totalMemory));
        response.setFreeMemory(formatBytes(freeMemory));
        response.setUsedMemory(formatBytes(usedMemory));

        return response;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public HostNameResponse getHostName(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final HostNameRequest request
    ) {
        @NotNull final HostNameResponse response = new HostNameResponse();
        @NotNull final String hostName = InetAddress.getLocalHost().getHostName();
        response.setHostName(hostName);
        return response;
    }

}