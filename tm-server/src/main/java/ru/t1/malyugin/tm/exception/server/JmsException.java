package ru.t1.malyugin.tm.exception.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.exception.AbstractException;

public final class JmsException extends AbstractException {

    public JmsException(@NotNull final String message) {
        super(message);
    }

}