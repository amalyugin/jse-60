package ru.t1.malyugin.tm.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.log.OperationEvent;
import ru.t1.malyugin.tm.log.OperationType;
import ru.t1.malyugin.tm.service.JmsLoggerService;

@Component
public final class EntityListener implements PostUpdateEventListener, PostDeleteEventListener, PostInsertEventListener {

    @NotNull
    private final JmsLoggerService jmsLoggerService;

    @Autowired
    public EntityListener(@NotNull final JmsLoggerService jmsLoggerService) {
        this.jmsLoggerService = jmsLoggerService;
    }

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent postDeleteEvent) {
        sendJmsMessage(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent postInsertEvent) {
        sendJmsMessage(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent postUpdateEvent) {
        sendJmsMessage(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    private void sendJmsMessage(
            @NotNull final OperationType type,
            final Object entity
    ) {
        jmsLoggerService.send(new OperationEvent(type, entity));
    }

}