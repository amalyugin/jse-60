package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.malyugin.tm.dto.model.AbstractUserOwnedDTOModel;

import java.util.Collection;
import java.util.List;

public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel> extends AbstractDTORepository<M>
        implements IUserOwnedDTORepository<M> {

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final String userId, @NotNull final Collection<M> modelList) {
        modelList.forEach(m -> add(userId, m));
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class).setParameter("userId", userId).getSingleResult();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.remove(model);
    }

}