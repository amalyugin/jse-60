package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.repository.dto.IDTORepository;
import ru.t1.malyugin.tm.api.service.dto.IDTOService;
import ru.t1.malyugin.tm.dto.model.AbstractDTOModel;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;

import java.util.List;

public abstract class AbstractDTOService<M extends AbstractDTOModel>
        implements IDTOService<M> {

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        getRepository().add(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        getRepository().update(model);
    }

    @Override
    public long getSize() {
        return getRepository().getSize();
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().clear();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().findOneById(id.trim());
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return getRepository().findAll();
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(id);
        if (model == null) return;
        remove(model);
    }

}