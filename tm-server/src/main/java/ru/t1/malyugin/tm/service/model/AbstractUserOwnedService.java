package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.malyugin.tm.api.service.model.IUserOwnedService;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel> extends AbstractService<M>
        implements IUserOwnedService<M> {

    @NotNull
    @Override
    protected abstract IUserOwnedRepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final String userId, @NotNull final M model) {
        getRepository().add(userId.trim(), model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final String userId, @NotNull final M model) {
        getRepository().update(userId.trim(), model);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return getRepository().getSize(userId);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        getRepository().clear(userId.trim());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().findOneById(userId.trim(), id.trim());
    }

    @Override
    public @NotNull List<M> findAll(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return getRepository().findAll(userId.trim());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return;
        remove(userId, model);
    }

}